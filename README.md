# Artifactory Sidekick

Bootstraps credentials for package management

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  # Login to internal Docker proxy
  - echo $PIPELINES_JWT_TOKEN | docker login --username=atlassian --password-stdin docker-proxy.services.atlassian.com
  # Run the pipe to generate the .buildeng directory
  - pipe: atlassian/artifactory-sidekick:v1
  # Install the config files to the home directory
  - source .artifactory/activate.sh
```

## Prerequisites

## Examples

Advanced example:

```yaml
- step:
    name: Use Sidekick
    script:
      # Login to internal Docker proxy
      - echo $PIPELINES_JWT_TOKEN | docker login --username=atlassian --password-stdin docker-proxy.services.atlassian.com
      - pipe: atlassian/artifactory-sidekick:v1
        variable:
          - SOX: "true"
      - source .artifactory/activate.sh
      - mvn org.apache.maven.plugins:maven-dependency-plugin:3.1.1:get -Dartifact=com.atlassian.buildeng:test-upload:1.0.76:jar
```

## Support
If you're reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
